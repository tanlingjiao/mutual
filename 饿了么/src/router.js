import Vue from "vue";
import Router from "vue-router";
import goods from "@/views/goods/goods.vue";


Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/goods",
      name: "goods",
      component: goods
    },
    {
      path: "/seller",
      name: "seller",
       component: () =>
       import(/* webpackChunkName: "seller" */"@/views/seller/seller.vue")
    },
    {
      path: "/ratings",
      name: "ratings",
      //动态导入
      component: () =>
        import(/* webpackChunkName: "ratings" */"@/views/ratings/ratings.vue")
    },
    {
      path: "/",
      redirect: '/goods'
    },
    {
      path: "*",
      redirect: '/goods'
    },
  ]
});
