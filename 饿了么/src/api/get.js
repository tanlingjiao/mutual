import request from "./request.js";

//   axios常见的配置选项：
// axios({
//  请求地址
// url: “请求地址”,
//  请求类型
// method: ‘get/post……’,
//  请求的根路径
// baseURL: “根路径”,
//  请求前的数据处理
// transformRequest: [function(data) {}],
//  请求后的数据处理
// transformResponse: [function(data) {}],
//  自定义的请求头
 // headers: { "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"},
//  URL查询对象，有1个同类的属性 data:{}一般配合post使用。params:{} 配合get使用
// params: {},
//  超时设置
// timeout:5000,
// })

//线上接口
// function getGoods() {
//   return request({
//     url: "/sell/api/goods",
//   });
// }


//本地json数据
//相当于在次执行axios({})

function getGoods() {
 return request({
   url: "goods.json",
 });
}


function getRatings() {
  return request({
    url: "ratings.json"
  });
}

function getSeller() {
  return request({
    url: "seller.json"
  });
}

export { getGoods, getRatings, getSeller };
