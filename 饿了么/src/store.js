import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    gather: [],
    detail:{}, //接收详情页数据
    detailShow:false, //控制详情显示和隐藏
    rightDom:null,
    // leftDom:null,
    foodHeight: [],  //存储右侧内容到顶部距离高度数组
    ind:0
  },
  mutations: {
    add(state, obj) {
     console.log(obj);
      let _index = state.gather.findIndex(item => item.name == obj.name)

      if (_index == -1) {
        obj.num = 1
        state.gather.push(obj)
      } else {
        state.gather[_index].num++
      }
    },
    //减号事件
    minus(state,obj) {
      let _index = state.gather.findIndex(item => item.name == obj.name)
      if(_index>-1){
        state.gather[_index].num--
        if(state.gather[_index].num==0){

          state.gather.splice(_index,1)
        }
      }

    },

    //点击详情页
    skipDetail(state,val){
      // console.log(val);
      state.detail=val
    // console.log(state.detail);

    },
    //获取右侧滚动盒子
    refCon(state,val){
      state.rightDom=val
      // console.log(state.rightDom);
    } ,
    //获取左侧导航栏下标
    ind(state,i){
      state.ind = i
      state.rightDom.scrollTop = state.foodHeight[i]
    },
    setArr(state,val){
      state.foodHeight = val
      // console.log(val);
    },
    setLeft(state){
      state.foodHeight.forEach((item,index)=>{
        if(state.rightDom.scrollTop>=item){
          state.ind = index
        }
      })
    }
  },
  actions: {},
  getters:{
   
  }
});