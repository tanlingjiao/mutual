import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'


// Vue.prototype.$http=axios

Vue.config.productionTip = false

// var BScroll = require('@better-scroll/scroll')
// import BScroll from '@better-scroll/core'

// import { createApp } from 'vue';
import Vant from "vant";
import "vant/lib/index.css";
Vue.use(Vant);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
